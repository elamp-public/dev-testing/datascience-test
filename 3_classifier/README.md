# Classifier

## Exercise (20min)

The purpose of this exercise is to propose a theorical implementation of a skills classifier detecting duplicate skills inside an instance.

We have the following requirements on the classifier model :
- being able to detect skills on every customer instances
- being able to handle skills precision and specificities of each customer (see Skills mapping specificities)

On the context of this exercise, propose a theorical implementation of a classifier containing the following informations :
- technical hypothesis => which model types you would test and why
- input data model to train the model
- metrics to monitor to check the model performance


## Skills Mapping specificities

Depending on the customer, skill mean something different. 

Let's take an example with IT skills on two different companies.

An **IT consulting Company** (instance 1) may have this IT skills mapping :
- IT
    - Programming Languages
        - JavaScript
        - JavaScript ES6
        - Golang
        - Node.js
        - Java 7
        - Java 8
        - Python 2
        - Python 3
    - Architecture
        - Virtualization
            - Kubernetes
            - KVM
            - VMWare
        - Operating System
            - Unix
                - Linux
                    - Ubuntu
                    - Debian
                    - CentOS
                - OSX
            - Microsoft
                - Windows 10

An **Industry Company** (instance 2) may have this IT skills mapping :
- IT
    - Programming Languages
        - JavaScript
        - Python
        - Java
    - Office
        - Operating System
            - Linux
            - Microsoft
            - OSX
        - Microsoft Office
            - Excel
            - Word
        - OpenOffice


The level of granularity is very different for these companies.


The classifier must be able to :
- detect Python2 / Python3 as duplicate on instance 2 but not on instance 1
- detect Java 7 / Java 8 as duplicate on instance 2 but not on instance 1
- detect Unix as duplicate of Linux on instance 1
- detect Unix as duplicate of Unix on instance 2  