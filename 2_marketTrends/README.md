# Market Trends

## Introduction

At eLamp, we run analysis on internal skills data we collect and enrich through users interactions.

Customers also need some overview about skills market trends :
- skills trends on the market :
    - is the skill demand decreasing/increasing on the market ?
    - is the skill offer decreasing/increasing on the market ?
- jobs trends on the market :
    - is the job demand decreasing/increasing on the market ?
    - is the job offer decreasing/increasing on the market ?
    - what is average salary for this job on the market ?
- ... to be defined

## Exercise (15min)

For the context of this exercise, you would :
- identify public data sources (4-5) that can provide these types of metrics and provide some basic theorical implementation (technical tools and methods)
- give 2-3 new metrics you have identified on this domain