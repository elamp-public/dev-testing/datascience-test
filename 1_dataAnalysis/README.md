# Data Analysis

## Introduction

The purpose of this exercise is to identify some analysis reports we can provide to our customers based on the data we have collected.

The collected data represents main data sources extracted from an eLamp instance and simplified for this exercise.

We provide the following files :
- `skill_libraries.json` : represents big skills domains used on the company. It can be "IT", "Marketing", "Sales", ... Libraries contains skills organized in graph
- `skills.json` : skills used in this instance
- `qualifiers.json` : qualifiers represent indicators attached to skills and used to describe user competencies. It can be a skill **Level** of expertise, or **years of experience** on the skill
- `users.json` : list of user profiles with their associated jobs and skills
- `userSkills.json` : list of user-skill relations

See Dataset description for more details

## Exercise (25min)

On the context of this exercise, you would :
- identify data analysis and data correlation present on the dataset
- propose technical methods you would use for this analysis (theorical data pipeline, output data model, ...)


*The purpose of this exercise is to suggest data analysis and methods to implement it (Not to be implemented)*

## Dataset description

### Libraries

**Library** object represents skills domain. It can be eihter IT, Marketing or Sales domain.

| Field name | Field type | Field description |
|------------|------------|-------------------|
| `id` | `string` | unique identifier |
| `kind` | `string` | specify the entity type. always `skills#library` |
| `name` | `string` | library name |
| `description` | `string` | library description |
| `aliases` | `string[]` | list of aliases string |
| `libraryContext` | `string` | unique identifier of the library context. always the id itself
| `createdAt` | `string` | iso-8601 creation date |
| `updatedAt` | `string` | iso-8601 last update date |

### Skills

**Skill** object is the main eLamp object. 
It describes a skill unit with a name, prettyName (short name) and description. 

*libraryContext* is the **Library** associated to the skill (eg. *Python* is in *IT* library)
*qualifiers* is a list of qualifiers attached to the **skill**. Attaching a **qualifier** to a skill provides the way for the user to evaluate his competency (eg. evaluate himself on the *Level* qualifier of *Python* skill)

| Field name | Field type | Field description |
|------------|------------|-------------------|
| `id` | `string` | unique identifier |
| `kind` | `string` | specify the entity type. always `skills#skill` |
| `name` | `string` | skill name |
| `prettyName` | `string` | skill short name |
| `description` | `string` | skill description |
| `aliases` | `string[]` | list of aliases string |
| `libraryContext` | `string` | unique identifier of the library context in which the skill resides |
| `logo` | `string` | skill logo |
| `color` | `string` | skill color |
| `customFields` | `object` | object containing custom attributes |
| `qualifiers` | `string[]` | list of qualifier identifiers attached to the skill |
| `parentSkills` | `string[]` | list of parent skills identifiers |
| `childSkills` | `string[]` | list of children skills identifiers |
| `provider` | `string` | define from which provider the skill was created. it can be **hosted** (eLamp) or **linkedin** |
| `createdAt` | `string` | iso-8601 creation date |
| `updatedAt` | `string` | iso-8601 last update date |

### Qualifiers

A **Qualifier** describes an evaluation indicator. If you attached a qualifier to a skill, user can evaluate his competency on this qualifier. It can be a *Level*, a *Expiration*, a *Certificate File*, ... everything that help better evaluate a skill.

| Field name | Field type | Field description |
|------------|------------|-------------------|
| `id` | `string` | unique identifier |
| `kind` | `string` | specify the entity type. always `skills#qualifier` |
| `name` | `string` | qualifier name |
| `description` | `string` | qualifier description |
| `fieldType` | `enum` | qualifier type : can be **MULTIVALUES**, **BOOLEAN**, **TEXT**, **NUMERIC** or **EXPIRATION** |
| `allowedValues` | `string[]` | if **fieldType** is **MULTIVALUES**, it specifies the allowed values |
| `createdAt` | `string` | iso-8601 creation date |
| `updatedAt` | `string` | iso-8601 last update date |

### Users

An **User** is a basic representation of an user profile.

| Field name | Field type | Field description |
|------------|------------|-------------------|
| `id` | `string` | unique identifier |
| `kind` | `string` | specify the entity type. always `skills#qualifier` |
| `displayName` | `string` | user's display name |
| `jobs` | `string[]` | users associated jobs |
| `createdAt` | `string` | iso-8601 creation date |
| `updatedAt` | `string` | iso-8601 last update date |

### Users skills

An **User skill** represent a link between an user (*userId*) and a skill (*skillId*).
List of *qualifiers* can be provided to further evaluate the skill (eg. specify a **Level**,...)

| Field name | Field type | Field description |
|------------|------------|-------------------|
| `id` | `string` | unique identifier |
| `kind` | `string` | specify the entity type. always `skills#power` |
| `userId` | `string` | user identifier |
| `skillId` | `string` | skill identifier |
| `qualifiers` | `{id: string, value: any}[]` | list of {id, value} containing each qualifier on which the user evaluated himself |